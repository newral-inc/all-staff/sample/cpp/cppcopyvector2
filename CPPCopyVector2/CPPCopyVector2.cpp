﻿#include "pch.h"
#include <iostream>
#include <vector>
#include <iterator>

int main()
{
	// コピー元のvectorを用意します。
	std::vector<int> src = { 1, 5, 13, 21 };

	// コピー先のvectorを用意します。
	std::vector<int> dst;

	// vectorをコピーします。
	std::copy(src.begin(), src.end(), std::back_inserter(dst));

	for (const auto& item : dst)
	{
		std::cout << item << std::endl;
	}
}
